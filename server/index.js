const express = require('express');
const app = express();
const fetch = require("node-fetch");
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.set('json spaces', 20);
// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));

// Task Model
//const Task = require("../models/taskModel");


// listen to GET requests on /hello
app.get('/brewe', function (req, res) {
    //res.json(brewerie);
    //res.send("cervezas");
    let url 	= 'https://api.openbrewerydb.org/breweries?sort=+name&per_page=10';
    //var fetch;
    fetch(url)
    .then(res => res.json())
    .then((out) => {
     //console.log('Checkout this JSON! ', out);
     //console.log('---------------------------------------------------- ');
   
        //res.json(out);
        res.json(out);
     

       
    })
   
    .catch(err => { throw err });
    
  

});


app.listen(3000, () => console.log(`Example app listening on port 3000!`))
